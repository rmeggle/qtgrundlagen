#include <QCoreApplication>
#include <QDebug>
#include <iostream>

void AnwendungsbeispielArrayOfInt_Classic();
void AnwendungsbeispielArrayOfInt_PointerArithmetik();
void Funktionszeiger();
int addiere(int a, int b);
int subtrahiere(int a, int b);
int dividiere(int a, int b);
int multipliziere(int a, int b);

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    int a1;
    int a2;

    // 15 wird an die Adresse von a2 geschrieben:
    a2=15;

    // der Wert von a2 wird nach a1 kopiert
    a1 = a2;

    // Anwendungsbeispiel: Array of int
    AnwendungsbeispielArrayOfInt_Classic();
    AnwendungsbeispielArrayOfInt_PointerArithmetik();

    // Fortgeschrittenes Beispiel: Anonyme Zeiger und Funktionszeiger (-> OOP -> delegate)
    Funktionszeiger();

    return a.exec();
}

void AnwendungsbeispielArrayOfInt_Classic() {

    int ZahlenFeld[100];
    for (int i = 0 ; i < 100 ; i++){
        ZahlenFeld[i]=i;
    }
}

void AnwendungsbeispielArrayOfInt_PointerArithmetik() {

    int ZahlenFeld[100];
    // ...code...

}


// -------------------------

int addiere(int a, int b){
    return a+b;
}

int subtrahiere(int a, int b){
    return a-b;
}

int dividiere(int a, int b){
    return a/b;
}

int multipliziere(int a, int b){
    return a*b;
}


void Funktionszeiger(){
    double e;
    // ...code...

}


