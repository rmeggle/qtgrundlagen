#include <QCoreApplication>
#include <QDebug>
#include <iostream>

void AnwendungsbeispielArrayOfInt_Classic();
void AnwendungsbeispielArrayOfInt_PointerArithmetik();
void Funktionszeiger();
int addiere(int a, int b);
int subtrahiere(int a, int b);
int dividiere(int a, int b);
int multipliziere(int a, int b);

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    int a1;
    int a2;

    qDebug() << "a1 ist deklariert und verweist auf die Adresse " << &a1;
    qDebug() << "a2 ist deklariert und verweist auf die Adresse " << &a2;

    // 15 wird an die Adresse von a2 geschrieben:
    a2=15;

    // der Wert von a2 wird nach a1 kopiert
    a1 = a2;

    qDebug() << "Wert in a1 ist " << a1 << " und verweist auf die Adresse " << &a1;
    qDebug() << "Wert in a1 ist " << a2 << " und verweist auf die Adresse " << &a2;

    int *z1;
    qDebug() << "Zeiger z1 ist deklariert und verweist auf die Adresse " << &z1;

    // Addresse von a1 nach z1 zuweisen:
    z1 = &a2;
    qDebug() << "Zeiger z1 verweist nun auf die Adresse " << &z1;
    *z1 = 42;
    qDebug() << a2;

    // Anwendungsbeispiel: Array of int
    AnwendungsbeispielArrayOfInt_Classic();
    AnwendungsbeispielArrayOfInt_PointerArithmetik();

    // Fortgeschrittenes Beispiel: Anonyme Zeiger und Funktionszeiger (-> OOP -> delegate)
    Funktionszeiger();

    return a.exec();
}

void AnwendungsbeispielArrayOfInt_Classic() {

    qDebug() << "AnwendungsbeispielArrayOfInt_Classic()";

    int ZahlenFeld[100];
    for (int i = 0 ; i < 100 ; i++){
        ZahlenFeld[i]=i;
    }
}

void AnwendungsbeispielArrayOfInt_PointerArithmetik() {

    qDebug() << "AnwendungsbeispielArrayOfInt_PointerArithmetik()";

    int ZahlenFeld[100];

    // ZahlenFeld ist vom Typ int => 4Byte
    for (int i = 0 ; i < 100 ; i++){
        // i ist ebenfalls von Typ int => 4Byte
        // ==> wir müssen um jeweils 4Byte (Größe von int) dereferenzieren
        *(ZahlenFeld + i ) = i;
    }

}


// -------------------------

int addiere(int a, int b){
    return a+b;
}

int subtrahiere(int a, int b){
    return a-b;
}

int dividiere(int a, int b){
    return a/b;
}

int multipliziere(int a, int b){
    return a*b;
}


void Funktionszeiger(){
    double e;

    int (*rechne)(int,int) = 0;

    rechne = &addiere;
    e = (int)(rechne(1,1));

    qDebug() << "1 + 1 = " << e;

    rechne = &subtrahiere;
    e = (int)(subtrahiere(1,1));

    qDebug() << "1 - 1 = " << e;

    rechne = &dividiere;
    e = (double)(dividiere(36,6));

    qDebug() << "36 / 6 = " << e;

    rechne = &multipliziere;
    e = (int)(multipliziere(6,6));

    qDebug() << "6 * 6 = " << e;



}


