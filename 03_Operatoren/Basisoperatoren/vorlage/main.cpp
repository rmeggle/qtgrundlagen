#include <QCoreApplication>
#include <QDebug>
#include <math.h>

void ArithmetischeOperatoren();
void RelationaleOperatoren();
void LogischeOperatoren();
void BitweiseOperatoren();
void ZeichenkettenOperatoren();

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    ArithmetischeOperatoren();
    RelationaleOperatoren();
    LogischeOperatoren();
    BitweiseOperatoren();

    return a.exec();
}


void ArithmetischeOperatoren()
{
    int x = 40;
    int y = 2;

    // ////////////////////////////////////////
    // Grundlegende Operatoren

    // Addition
    int summe = x + y;

    // Subtraktion
    int differenz = x - y;

    // Multiplikation

    int produkt = x * y;

    // Division

    // Vorsicht... die  Variable ist nun ein int und kein double!!
    // Wie verhält es sich generell, wenn int durch int dividiert werden?
    int quotient = x / y;

    // Modulo
    int restwert= x % y;

    // ////////////////////////////////////////
    // Auflösungsregeln
    // Klammer vor Punkt vor Strich, aber ...
    int wasIst1 = 6 / 2 * 3;
    int wasIst2 = 6 / 2 * (1 + 2);

    // ////////////////////////////////////////
    // allgem. Kurzschreibweisen"
    x += 1;// entspricht x = x + 1
    x -= 1;// entspricht x = x - 1
    x *= 1;// entspricht x = x * 1
    x /= 1;// entspricht x = x / 1

    // ////////////////////////////////////////
    // Kurzschreibweise: Post- und Preinkrement

    // Häufig fehlinterpretiert: Diese beiden Zuweisungen eines
    // post/preinkrementes sind identisch: In beiden Fällen ist der
    // Wert in y 2.
    x = 1;  y = x++;
    x = 1;  y = ++x;

    // Anders verhält es sich bei unmittelbarer Verwendung als z.B.
    // Parameter - hier als Parameter für die Methode WriteLine
    x = 1;
    qDebug() << x++; // Ausgabe "1"
    x = 1;
    qDebug() << ++x; // Ausgabe "2"

    // oder bei dem Verwenden als Index in einem Array:
    char buchstaben[] = { 'a', 'b', 'c' };
    int index = 0;
    qDebug() <<  buchstaben[index];
    index = 1; qDebug() << buchstaben[index++]; // Ausgabe: b
    index = 1; qDebug() << buchstaben[++index]; // Ausgabe: c

}

void RelationaleOperatoren()
{
    int x = 47;
    int y = 11;
    bool result;

    // gleich
    result = x == y;

    // ungleich
    result = x != y;

    // größer
    result = x > y;

    // kleiner
    result = x < y;

    // größer gleich
    result = x >= y;

    // kleiner gleich
    result = x <= y;

}

void LogischeOperatoren()
{
    bool x = true;
    bool y = false;
    bool result;

    // UND:
    result = x && y;
    // ODER:
    result = x || y;
    // XOR ( exklusives ODER ) - oft verwechselt mit exponent! Exponent ist in C#: Math.Pow():
    result = x ^ y; // VORISICHT: ^ ist nicht Exponent, sondern eine XOR-Verknüpfung!!

    double exp = pow(2,0);
    // NICHT
    result = !x;
}

void BitweiseOperatoren()
{
    int x = 1; // Binär: 000001
    int y = 3; // Binär: 000010
    int result;

    // und ( Ergebnis 1 wenn beide bit 1 sind)
    result = x & y;
    // oder ( Ergebnis 1 wenn (mind.) eines der beiden bit 1 ist)
    result = x | y;
    // xor ( Ergebnis 1 wenn genau eines der beiden bit 1 ist)
    result = x ^ y;
    // nicht  (Ergebnis 1 wenn das bit 0 und umgekehrt ist)
    result = ~y;
    // verschieben nach links
    result = x << 1;
    // verschieben nach rechts
    result = x >> 1;


}



