#include <QCoreApplication>
#include <QDebug>
#include <QTextStream>

#include <QFile>
#include <QTemporaryFile>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    // QFile                       Zugriff auf Dateien (lokal und embedded)
    // QTemporaryFile              Zugriff auf tmp-Dateien(lokal)

    // Weiter interessant bei IO's sind:

    // QBuffer                     Liest/Schreibt Daten in ein QByteArray
    // QProcess                    Startet externen handler (inter-process communication)
    // QTcpSocket                  Transfers "stream of data" über Netzwerk (TCP)
    // QUdpSocket                  Senden/Empfangen UDP datagrams über das Netzwerk
    // QSslSocket                  Transfers encrypted data stream über Netzwkerk (SSL/TLS)


    QFile datei("meineDatei.txt");

    if (!datei.open(QIODevice::WriteOnly)) {
        qDebug() << "Kann Datei nicht zum schreiben öffnen!";
    }  else {
        QTextStream dateiinhalt(&datei);
        dateiinhalt.setCodec("UTF-8");
        dateiinhalt << "Donald Duck" << endl;
        dateiinhalt << "Dagobert Duck"<< endl;
    }
    datei.close();

    // Anfügen: ---------------------------------
    //                                          |
    //                                          *
    if (!datei.open(QIODevice::WriteOnly | QIODevice::Append)) {
        qDebug() << "Kann Datei nicht zum schreiben öffnen!";
    }  else {
        QTextStream dateiinhalt(&datei);
        dateiinhalt.setCodec("UTF-8");
        dateiinhalt << "Daniel Düsentrieb" << endl;
        dateiinhalt << "Gustav Ganz"<< endl;
    }
    datei.close();

    // Lesen
    QTextStream in(&datei);
    if (!datei.open(QIODevice::ReadOnly)) {
        qDebug() << "Kann Datei nicht zum lesen öffnen!";
    } else {
    while (!in.atEnd()){
            QString zeile=in.readLine();
            qDebug() << zeile;
        }
    }
    datei.close();

    qDebug() << "-----------------";

    //QTemporaryFile:
    QString tmpFileName;
    QTemporaryFile tmpDatei;

    // Schreiben:
    if (!tmpDatei.open()) {
        qDebug() << "Kann Datei nicht zum schreiben öffnen!";
    } else {
        tmpFileName = tmpDatei.fileName();
        QTextStream tmpDateiinhalt(&tmpDatei);
        tmpDateiinhalt.setCodec("UTF-8");
        tmpDateiinhalt << "Daniel Düsentrieb" << endl;
        tmpDateiinhalt << "Gustav Ganz"<< endl;
    }
    tmpDatei.close();

    // Lesen:
    QFile tmpdatei(tmpFileName);
    QTextStream tmpin(&tmpdatei);
    if (!tmpdatei.open(QIODevice::ReadOnly)) {
        qDebug() << "Kann Datei nicht zum lesen öffnen!";
    } else {
    while (!tmpin.atEnd()){
            QString zeile=tmpin.readLine();
            qDebug() << zeile;
        }
    }
    tmpdatei.close();

    return a.exec();
}

