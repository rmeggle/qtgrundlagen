#include <QCoreApplication>
#include <QDebug>
#include <QTextStream>

#include <QFile>
#include <QTemporaryFile>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    // QFile                       Zugriff auf Dateien (lokal und embedded)
    // QTemporaryFile              Zugriff auf tmp-Dateien(lokal)

    return a.exec();
}

