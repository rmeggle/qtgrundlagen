#include <QCoreApplication>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    int i=0;
    int i1=10;

    // Kopfgesteuerte Schleifen

    while (i<=i1) {
        i++;
        // ...
    }

    // Fussgesteuerte Schleife
    do {
        i++;
        // ...
    } while (i<=i1);


    // for-next-Schleife
    for (int u=0; u<=i1 ; u++) {
        // ...
    }

    // Das kontrollierte unterbrechen von Schleifen
    for (int u=0; u<=i1 ; u++) {
        // ...
        if (u>=8) break; // Abbrechen, wenn Bedinung erfüllt ist
    }


    for (int u=0; u<=i1 ; u++) {
        qDebug() << u;
        if (u>=8) continue; // Abbrechen, wenn Bedinung erfüllt ist
        qDebug() << "in der Schleife.."; // Wird nicht mehr abgearbeitet, wenn u>=8 ist
    }

    // NEVER USE GOTO um aus einer Schleife auszuscheren!

    // for (int u=0; u<=i1 ; u++) {
    //     // ...
    //     if (u>=8) goto weiter; // Abbrechen, wenn Bedinung erfüllt ist
    // }
    //
    // weiter:


    return a.exec();
}

