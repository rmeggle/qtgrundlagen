#include <QCoreApplication>
#include <QDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    int x = 10;
    int y = 20;

    qDebug() << "x: " << x << " ; y: " << y;

    return a.exec();
}

