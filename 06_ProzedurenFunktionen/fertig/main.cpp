#include <QCoreApplication>
#include <QDebug>

// Beispiel: Funktion mit Call by Value
int addiereByValue(int a, int b) {
    return a+b;
}

// Beispiel: Funktion mit Call by Reference
int addiereByReference(int &a, int &b) {
    return a+b;
}

// Beispiel: "Prozedur" mit Call by Reference
void tauscheByReference(int &a, int &b) {
    int tmp = a;
    a = b;
    b = tmp;
}

// Beispiel: "Prozedur" mit Call by Pointer
void tauscheByPointer(int *a, int *b) {
    int tmp = *a;
    *a = *b;
    *b = tmp;
}


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    int x = 10;
    int y = 20;

    qDebug() << "x: " << x << " ; y: " << y;

    tauscheByPointer(&x,&y);

    qDebug() << "x: " << x << " ; y: " << y;

    tauscheByReference(x,y);

    qDebug() << "x: " << x << " ; y: " << y;

    qDebug() << "x + y = " << addiereByValue(x,y);
    qDebug() << "x + y = " << addiereByReference(x,y);

    return a.exec();
}

