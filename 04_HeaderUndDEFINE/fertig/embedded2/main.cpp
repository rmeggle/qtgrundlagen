#include <QCoreApplication>
#include <QDebug>

// Deklaration der Funktion vor main, und sie...
void Ausgabe(QString Text);

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Ausgabe("Hallo Welt");

    return a.exec();
}


// ...können dann auch danach genannt werden.
void Ausgabe(QString Text) {

    qDebug() << Text;

}
