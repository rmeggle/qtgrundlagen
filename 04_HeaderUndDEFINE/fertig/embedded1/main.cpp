#include <QCoreApplication>
#include <QDebug>


// Alle Funktionen sind vor(!) dem main genannt!
void Ausgabe(QString Text) {

    qDebug() << Text;

}


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Ausgabe("Hallo Welt");

    return a.exec();
}

