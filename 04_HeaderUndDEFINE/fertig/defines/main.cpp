#include <QCoreApplication>
#include <QDebug>

#define getmax(a,b) ((a)>(b)?(a):(b))


#ifndef MAXSIZE
#define MAXSIZE 3
#endif


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    qDebug() << getmax(20,40);

    for (int i = 1 ; i <= MAXSIZE; i++ )
        qDebug() << i;

    // Vordefinierte Macro's
    qDebug() << __LINE__;               // Aktuelle Zeile im Source Code
    qDebug() << __FILE__;               // Name der aktuellen Sourcecodedatei
    qDebug() << __DATE__;               // Startzeitpunkt (Datum) der Compillierung
    qDebug() << __TIME__;               // Startzeitpunkt (Uhrzeit) der Compillierung
    qDebug() << __cplusplus;            // c++ Version ( 199711L: ISO C++ 1998/2003 || 201103L: ISO C++ 2011)
    qDebug() << __STDC_HOSTED__;        // hosted == true <=> alle headerdateien verfügbar sind
    qDebug() << __STDC_ISO_10646__;     // Datum des Unicode-Standards
    qDebug() << __STDC_NO_THREADS__;    // true <-> wenn keine Threads im Programm verfügbar sind

    return a.exec();
}

