#include <QCoreApplication>
#include <iostream>
#include <QDebug>
#include <QString>
#include <QStringList>
#include <QDate>
#include <QTime>
#include <QLinkedList>


void ISO_Basistypen();
void QT_Basistypen();

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);


    // //////////////////  c++ Basistypen /////////////////////////////
    // c++ kennt acht grundlegende Datentypen:
    // * bool                           true/false
    // * signed|unsigned char           Zeichen
    // * signed|unsigned int            ganzzahliger Datentypen
    // * signed|unsigned short          ganzzahliger Datentypen
    // * signed|unsigned long           ganzzahliger Datentypen
    // * signed|unsigned float          Fließkommatyp
    // * signed|unsigned double         Fließkommatyp
    // * signed|unsigned long double    Fließkommatyp

    // Beispiel:
    ISO_Basistypen();std::cout << "Beispiele der ISO Basistypen";
    // ///////////////////////////////////////////////////////////////

    // //////////////////  QT Basistypen /////////////////////////////
    //
    //
    // action       Typ mit allen Eigenschaften des QAction
    // bool         true/false mit default auf false
    // color        Vereinfachung der Farbschreibweise ("streelblue" statt "RGBA")
    // date         Ein Datumstyp  ("YYYY-MM-DD")
    // double       wie C++ ISO
    // enumeration  Aufzählungstyp
    // font         Eigenschaftstyp für QFont
    // int          wie C++ ISO
    // list         Eine Liste von Objekten
    // point        Ein Punkt mit x und y Attributen
    // real         Fliesskommatyp im IEEE floating point Format
    // rect         Ein Typ mit x,y Höhe und Breite-Attribute
    // size         Ein Typ mit einem Breiten- und Höhenattribut
    // string       Ein String-Datetyp
    // time         Ein Zeittyp ("hh:mm:ss")
    // url          Ein Ressourcelocator (fuer Web/Dateiname)
    // variant      Ein Varianter Datentyp
    // vector3d     Ein 3D-Vektortyp mit x, y, und z Attribut


    // qt Erweitert und Spezifiziert hier.
    // Zu den wichtigsten gehören:

    // qint8            ->  signed byte
    // qint16           ->  signed 16-bit integer
    // qint32           ->  signed 32-bit integer
    // qint64           ->  signed 64-bit integer
    // quint8           ->  unsigned byte
    // quint16          ->  unsigned 16-bit integer
    // quint32          ->  unsigned 32-bit integer
    // quint64          ->  unsigned 64-bit integer

    // Wenn statt "q" ein "Q" steht, handelt es sich um
    // Typ-Klassen -> OOP

    // QBitArray        -> Array von bits
    // QByteArray       -> Array von Bytes
    // QDateTime        -> Datums und Zeitfunktionen
    // QLinkedList<T>   -> verkette Liste von Objekten
    // QList<T>         -> Liste von Objekten
    // QString          -> null == 0xFFFFFFFF (quint32) || UTF16
    // QTime            -> Millisekunden seit Mitternacht (quint32)
    // QVariant         -> "passt scho"

    // Vollständige Liste gibt es hier:
    // http://doc.qt.io/qt-4.8/datastreamformat.html

    // QT spezifische Definitionen aus <QtCore/qconfig.h>
    //
    // QT_VERSION_STR
    // QT_VERSION
    // QT_PACKAGEDATE_STR
    // Q_OS_UNIX
    // Q_CONSTRUCTOR_FUNCTION(AFUNC)   static const int AFUNC ## __init_variable__ = AFUNC();
    // Q_DESTRUCTOR_FUNCTION(AFUNC)
    // Q_REQUIRED_RESULT
    // Q_WS_X11
    // Q_INT64_C(c)   static_cast<long long>(c ## LL)
    // Q_UINT64_C(c)   static_cast<unsigned long long>(c ## ULL)
    // Q_INIT_RESOURCE(name)
    // Q_CLEANUP_RESOURCE(name)

    // Beispiele:
    QT_Basistypen();
    // ///////////////////////////////////////////////////////////////

    return a.exec();
}

void ISO_Basistypen()  {


}

void QT_Basistypen() {


}

