#include <QCoreApplication>
#include <iostream>
#include <QDebug>
#include <QString>
#include <QStringList>
#include <QDate>
#include <QTime>
#include <QLinkedList>


void ISO_Basistypen();
void QT_Basistypen();

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);


    // //////////////////  c++ Basistypen /////////////////////////////
    // c++ kennt acht grundlegende Datentypen:
    // * bool                           true/false
    // * signed|unsigned char           Zeichen
    // * signed|unsigned int            ganzzahliger Datentypen
    // * signed|unsigned short          ganzzahliger Datentypen
    // * signed|unsigned long           ganzzahliger Datentypen
    // * signed|unsigned float          Fließkommatyp
    // * signed|unsigned double         Fließkommatyp
    // * signed|unsigned long double    Fließkommatyp

    // Beispiel:
    ISO_Basistypen();std::cout << "Beispiele der ISO Basistypen";
    // ///////////////////////////////////////////////////////////////

    // //////////////////  QT Basistypen /////////////////////////////
    //
    //
    // action       Typ mit allen Eigenschaften des QAction
    // bool         true/false mit default auf false
    // color        Vereinfachung der Farbschreibweise ("streelblue" statt "RGBA")
    // date         Ein Datumstyp  ("YYYY-MM-DD")
    // double       wie C++ ISO
    // enumeration  Aufzählungstyp
    // font         Eigenschaftstyp für QFont
    // int          wie C++ ISO
    // list         Eine Liste von Objekten
    // point        Ein Punkt mit x und y Attributen
    // real         Fliesskommatyp im IEEE floating point Format
    // rect         Ein Typ mit x,y Höhe und Breite-Attribute
    // size         Ein Typ mit einem Breiten- und Höhenattribut
    // string       Ein String-Datetyp
    // time         Ein Zeittyp ("hh:mm:ss")
    // url          Ein Ressourcelocator (fuer Web/Dateiname)
    // variant      Ein Varianter Datentyp
    // vector3d     Ein 3D-Vektortyp mit x, y, und z Attribut


    // qt Erweitert und Spezifiziert hier.
    // Zu den wichtigsten gehören:

    // qint8            ->  signed byte
    // qint16           ->  signed 16-bit integer
    // qint32           ->  signed 32-bit integer
    // qint64           ->  signed 64-bit integer
    // quint8           ->  unsigned byte
    // quint16          ->  unsigned 16-bit integer
    // quint32          ->  unsigned 32-bit integer
    // quint64          ->  unsigned 64-bit integer

    // Wenn statt "q" ein "Q" steht, handelt es sich um
    // Typ-Klassen -> OOP

    // QBitArray        -> Array von bits
    // QByteArray       -> Array von Bytes
    // QDateTime        -> Datums und Zeitfunktionen
    // QLinkedList<T>   -> verkette Liste von Objekten
    // QList<T>         -> Liste von Objekten
    // QString          -> null == 0xFFFFFFFF (quint32) || UTF16
    // QTime            -> Millisekunden seit Mitternacht (quint32)
    // QVariant         -> "passt scho"

    // Vollständige Liste gibt es hier:
    // http://doc.qt.io/qt-4.8/datastreamformat.html

    // QT spezifische Definitionen aus <QtCore/qconfig.h>
    //
    // QT_VERSION_STR
    // QT_VERSION
    // QT_PACKAGEDATE_STR
    // Q_OS_UNIX
    // Q_CONSTRUCTOR_FUNCTION(AFUNC)   static const int AFUNC ## __init_variable__ = AFUNC();
    // Q_DESTRUCTOR_FUNCTION(AFUNC)
    // Q_REQUIRED_RESULT
    // Q_WS_X11
    // Q_INT64_C(c)   static_cast<long long>(c ## LL)
    // Q_UINT64_C(c)   static_cast<unsigned long long>(c ## ULL)
    // Q_INIT_RESOURCE(name)
    // Q_CLEANUP_RESOURCE(name)

    // Beispiele:
    QT_Basistypen();
    // ///////////////////////////////////////////////////////////////

    return a.exec();
}

void ISO_Basistypen()  {

    std::cout << "Beispiele der ISO Basistypen";

    // Ausgabe der Typgroessen:
    // qDebug() << sizeof(int);
    // qDebug() << sizeof(double);
    // qDebug() << sizeof(long);


    // ///////////////////////////////////////////////////////////////
    // Boolean
    bool nflag;             // nflag wird initialisiert, aber was?
    bool flag = true;       // true=1 && false=0
    int iflag = (int)flag;  // typecast von bool nach int
    int bflag = (bool)iflag;// typecast von int nach bool


    // ///////////////////////////////////////////////////////////////
    // Zeichen
    char ch01(97);   // Zeichenkette aus ASCII-Tabelle nr. 97 dec.
    char ch02('a');  // Zeichenkette ch2 enthält "a"

    // ///////////////////////////////////////////////////////////////
    // Zeichenketten
    char ch03[9];
    ch03[0] = 'H';
    ch03[1] = 'a';
    ch03[2] = 'l';
    ch03[3] = 'l';
    ch03[4] = 'o';
    ch03[5] = ' ';
    ch03[6] = 'W';
    ch03[7] = 'e';
    ch03[8] = 'l';
    ch03[9] = 't';
    ch03[10] = '\0';
    //qDebug(ch03);

    char ch04[] = {'H','a','l','l','o',' ','W','e','l','t','\0'};
    //qDebug(ch04);

    char ch05[] = "Hallo Welt";
    //qDebug(ch05);

    char ch06[20];
    strcpy (ch06,"Hallo Welt");
    //qDebug(ch06);

    strcpy (ch06,ch03);
    //qDebug(ch06);

    // ///////////////
    // Weitere, hier interessante String-Funktionen
    // aus der string.h sind:
    // * strcat
    // * strcmp
    // * strcpy
    // * strlen
    // (Hinweis: char* ist das gleiche/selbe wie char[])
    // ///////////////


    // ///////////////////////////////////////////////////////////////
    // ganzzahlige Datentypen
    int j;          // j wird initialisiert, Inhalt ist zufällig
    int i = 4711;   // Wert wird bei Initialisierung fest vergeben
    int y = 0;      // Wert wird bei Initialisierung auf 0 gesetzt

    //qDebug(y);

    // ///////////////////////////////////////////////////////////////
    // Fließkommatypen
    float fd;
    double dd;
    long double ld;
    int n(5);       // 5=> Integer
    double d(5.0);  // 5.0 => Fliesskomma
    float f1(5.0f);  // 5.0 => Fliesskomma, f suffix => float

    //qDebug() << f1 ;

}

void QT_Basistypen() {


    // ///////////////////////////////////////////////////////////////
    // Zeichenketten (Strings)
    QString str = "Hallo Welt";
    qDebug() << str;
    // Stringfunktionen (Methoden der Klasse QString)
    // size()
    //qDebug() << "Anzahl von Zeichen in" << str << "sind " << str.size();

    // -----------------
    // replace()
    QString str1 = str.replace("l","j");
    //qDebug() << str1;

    // -----------------
    // lastIndexOf()
    QString search = " ";
    int p = str.lastIndexOf(search);
    // Ausgabe mit typecast int -> QString
    // qDebug() << QString::number(p);
    // Ausgabe mit typecast toString (bei QVariant)
    // QVariant var(p);
    // qDebug() << var.toString();

    // -----------------
    // append()
    QString strAppend1 = "Hallo";
    QString strAppend2 = " ";
    QString strAppend3 = "Welt";
    QString strAppend4 = strAppend1.append(strAppend2.append(strAppend3));
    //qDebug() << strAppend4;


    // ///////////////////////////////////////////////////////////////
    // ganzzahlige Datentypen

    //qint8   // (signed char) && Typ ist garantiert ein 8-Bit auf allen Plattformen
    //qint16   // (signed short) && Typ ist garantiert ein 16-Bit auf allen Plattformen
    //qint23   // (signed int) && Typ ist garantiert ein 32-Bit auf allen Plattformen
    //qint64   // (signed long) && Typ ist garantiert ein 64-Bit auf allen Plattformen

    qint8 qint00 = 932838457459459;

    // Beispiel:
    qint64 qint01 = Q_INT64_C(932838457459459);
    qint64 qint02 = Q_UINT64_C(932838457459459);

    // ///////////////////////////////////////////////////////////////
    // QDateTime        -> Datums und Zeitfunktionen
    // Link: http://doc.qt.io/qt-5/qdate.html#

    QDate d1(2017,12,24);
    //qDebug() << "Weihnachten ist am " << d1;
    //qDebug() << "Weihnachten ist am " << d1.toString();
    //qDebug() << "Weihnachten ist am " << d1.toString("dd.MM.yyyy");
    //qDebug() << "64 Tage nach Weihnachten ist der " << d1.addDays(64).toString("dd.MM.yyyy");
    QDate d2 = QDate::currentDate();
    //qDebug() << "Heute ist der " << d2.toString("dd.MM.yyyy");
    //qDebug() << "...und es sind noch " << d2.daysTo(d1) << " Tage bis Weihnachten";


    // ///////////////////////////////////////////////////////////////
    // QTime            -> Millisekunden seit Mitternacht (quint32)
    QTime t1 = QTime(15,12,16); // 15:12:16
    QTime t2 = QTime(17,13,03); // 17:13:03
    int AnzahlSekunden = t1.secsTo(t2);
    //qDebug() << "Anzahl Sekunden von " << t1.toString("hh:mm:ss") << "bis " << t2.toString("hh:mm:ss") << " sind :" << AnzahlSekunden;



    // ///////////////////////////////////////////////////////////////
    // ////////////////         Listen      //////////////////////////
    // ///////////////////////////////////////////////////////////////
    // QStringList      -> Liste von Strings
    QStringList Mitarbeiterliste;
    Mitarbeiterliste << "Donald Duck";
    Mitarbeiterliste << "Dasy Duck";
    Mitarbeiterliste << "Daniel Düsentrieb";

    //for (int i = 0 ; i < Mitarbeiterliste.length(); ++i) {
    //    QString m = Mitarbeiterliste.at(i);
    //    qDebug() << Mitarbeiterliste.at(i);
    //
    //    m = Mitarbeiterliste.at(i);
    //    m.replace(QString::fromLatin1("ü"), "&uuml;");
    //    qDebug() << m;
    //
    //}



    // ///////////////////////////////////////////////////////////////
    // QList<T>         -> Liste von Objekten
    QList<QString> Mitarbeiterliste1;
    Mitarbeiterliste1 << "Donald Duck";
    Mitarbeiterliste1 << "Dasy Duck";
    Mitarbeiterliste1 << "Daniel Düsentrieb";

    QList<QDate> Datumsliste;
    QDate Heute = QDate::currentDate();
    QDate Weihnachten(2017,12,24);

    Datumsliste << Weihnachten;
    Datumsliste << Heute;

    // Liste sortieren;
    qSort(Datumsliste);

    //for (int i = 0 ; i < Datumsliste.length(); ++i) {
    //    qDebug() << Datumsliste.at(i).toString("dd.MM.yyyy");
    //}


    // ///////////////////////////////////////////////////////////////
    // QLinkedList<T>   -> verkette Liste von Objekten

    QLinkedList<QString> Mitarbeiterliste2;
    Mitarbeiterliste2 << "Donald Duck";
    Mitarbeiterliste2 << "Dasy Duck";
    Mitarbeiterliste2 << "Daniel Düsentrieb";



}

