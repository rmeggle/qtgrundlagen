#ifndef PERSON_H
#define PERSON_H

#include <QString>

class person
{
private:
    QString Vorname;
    QString Nachname;
    int Alter;

public:
    person(); // Konstruktor
    person (QString VN, QString NN, int a);
    ~person(); // Destructor

    QString getVorname();
    void setVorname(QString VN);

    QString getNachname();
    void setNachname(QString VN);

    int getAlter();
    void setAlter(int VN);

    int AlterInJahren();
    int ErrechnetesGeburtsjahr();

};

#endif // PERSON_H
