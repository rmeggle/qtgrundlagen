#include "person.h"
#include <QDate>

// Constructor:
person::person()
{

}

person::person (QString VN, QString NN, int a) {
    this->Vorname = VN;
    this->Nachname = NN;
    this->Alter=a;
}

// Destructor
person::~person()
{

}

int person::AlterInJahren() {
    return this->Alter;
}


int person::ErrechnetesGeburtsjahr() {
    int aktuellesJahr = QDate::currentDate().year();
    return aktuellesJahr - this->Alter;
}

QString person::getVorname() {
    return this->Vorname;
}

void person::setVorname(QString value) {
    this->Vorname = value;
}

QString person::getNachname() {
    return this->Nachname;
}

void person::setAlter(int value) {
    this->Alter = value;
}

int person::getAlter() {
    return this->Alter;
}

void person::setNachname(QString value) {
    this->Nachname = value;
}
