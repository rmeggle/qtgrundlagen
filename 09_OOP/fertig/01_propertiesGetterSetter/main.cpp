#include <QCoreApplication>
#include <QDebug>
#include "person.h"

#include <QList>


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    person p; // Erstellen einer Instanz von person
    p.setVorname("Donald");
    p.setNachname("Duck");
    p.setAlter(47);

    qDebug() << p.getNachname() << p.getVorname() << " ist " << p.getAlter()<< " Jahre alt";
    qDebug() << "Er ist " << p.AlterInJahren() << " Jahre alt";
    qDebug() << "Sein errechnetes Geburtsjahr ist: " << p.ErrechnetesGeburtsjahr();

    return a.exec();
}

