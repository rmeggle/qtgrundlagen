#include <QCoreApplication>
#include <QDebug>
#include "person.h"

#include <QList>


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    person p; // Erstellen einer Instanz von person
    p.setVorname("Donald");
    p.setNachname("Duck");
    p.setAlter(47);
    p.anzahlMenschen = 1;

    qDebug() << "--------------" << p.anzahlMenschen << "--------------";
    qDebug() << p.getNachname() << p.getVorname() << " ist " << p.getAlter()<< " Jahre alt";
    qDebug() << "Er ist " << p.AlterInJahren() << " Jahre alt";
    qDebug() << "Sein errechnetes Geburtsjahr ist: " << p.ErrechnetesGeburtsjahr();



    person p1; // Erstellen einer Instanz von person
    p1.setVorname("Dagogert");
    p1.setNachname("Duck");
    p1.setAlter(55);
    p.anzahlMenschen++;

    qDebug() << "--------------" << p.anzahlMenschen << "--------------";
    qDebug() << p1.getNachname() << p1.getVorname() << " ist " << p1.getAlter()<< " Jahre alt";
    qDebug() << "Er ist " << p1.AlterInJahren() << " Jahre alt";
    qDebug() << "Sein errechnetes Geburtsjahr ist: " << p1.ErrechnetesGeburtsjahr();

    return a.exec();
}

