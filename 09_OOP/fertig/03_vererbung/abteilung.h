#ifndef ABTEILUNG_H
#define ABTEILUNG_H
#include <QString>

#include "person.h" // <-- nicht vergessen!

class abteilung: public person
{
private:
    QString Abteilung;

public:
    abteilung();

    QString getAbteilung();
    void setAbteilung(QString VN);

};

#endif // ABTEILUNG_H
