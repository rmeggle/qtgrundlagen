QT += core
QT -= gui

TARGET = Constructor
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    person.cpp \
    abteilung.cpp

HEADERS += \
    person.h \
    abteilung.h

