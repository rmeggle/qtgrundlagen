#include <QCoreApplication>
#include <QDebug>
#include "abteilung.h"

#include <QList>


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    abteilung a0; // Erstellen einer Instanz von person
    a0.setVorname("Donald");
    a0.setNachname("Duck");
    a0.setAbteilung("Arbeitssuchend");
    a0.setAlter(47);

    a0.anzahlMenschen = 1;

    qDebug() << "--------------" << a0.anzahlMenschen << "--------------";
    qDebug() << a0.getNachname() << a0.getVorname() << " ist " << a0.getAlter()<< " Jahre alt";
    qDebug() << "Er ist " << a0.AlterInJahren() << " Jahre alt";
    qDebug() << "Sein errechnetes Geburtsjahr ist: " << a0.ErrechnetesGeburtsjahr();



    abteilung a1; // Erstellen einer Instanz von person
    a1.setVorname("Dagogert");
    a1.setNachname("Duck");
    a1.setAlter(55);
    a1.setAbteilung("Geldspeicher");
    a1.anzahlMenschen++;

    qDebug() << "--------------" << a1.anzahlMenschen << "--------------";
    qDebug() << a1.getNachname() << a1.getVorname() << " ist " << a1.getAlter()<< " Jahre alt";
    qDebug() << "Er ist " << a1.AlterInJahren() << " Jahre alt";
    qDebug() << "Sein errechnetes Geburtsjahr ist: " << a1.ErrechnetesGeburtsjahr();

    return a.exec();
}

