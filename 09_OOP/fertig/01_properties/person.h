#ifndef PERSON_H
#define PERSON_H

#include <QString>

class person
{

public:
    person(); // Konstruktor
    ~person(); // Destructor
    QString Vorname;
    QString Nachname;
    int Alter;
    int AlterInJahren();
    int ErrechnetesGeburtsjahr();

};

#endif // PERSON_H
