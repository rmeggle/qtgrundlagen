#include <QCoreApplication>
#include <QDebug>
#include "person.h"

#include <QList>


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    person p; // Erstellen einer Instanz von person
    p.Vorname="Donald";
    p.Nachname="Duck";
    p.Alter = 47;

    qDebug() << p.Nachname << p.Vorname << " ist " << p.Alter << " Jahre alt";
    qDebug() << "Er ist " << p.AlterInJahren() << " Jahre alt";
    qDebug() << "Sein errechnetes Geburtsjahr ist: " << p.ErrechnetesGeburtsjahr();

    return a.exec();
}

