#include "person.h"
#include <QDate>

// Constructor:
person::person()
{

}

// Destructor
person::~person()
{

}


int person::AlterInJahren() {
    return this->Alter;
}


int person::ErrechnetesGeburtsjahr() {
    int aktuellesJahr = QDate::currentDate().year();
    return aktuellesJahr - this->Alter;
}
