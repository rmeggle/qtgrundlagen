#include <QCoreApplication>

// Header für cout
#include <iostream>

// Header für qDebug()
#include <QDebug>


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    // c++ standard
    std::cout << "cout sagt: Hallo Welt" << std::endl;

    // mit QT Debug-Libary
    qDebug() << "qDebug sagt: Hallo Welt";

    // mit QTextStream-Libary
    QTextStream(stdout) << "QTextStream sagt: Hallo Welt" << endl;

    QTextStream cout(stdout, QIODevice::WriteOnly);
    cout << "QTextStream sagt nochmal: Hallo Welt" << endl;

    return a.exec();
}

