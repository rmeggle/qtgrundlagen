#include <QCoreApplication>

#include <QDebug>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlTableModel>
#include <QSqlRecord>

bool demoConnection();

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    demoConnection();

    return a.exec();
}

bool demoConnection()
{

    // QDB2        IBM DB2 version 7.1and later
    // QIBASE      Borland InterBase
    // QOCI        Oracle (Oracle Call Interface)
    // QODBC       ODBC (includes Microsoft SQL Server)
    // QPSQL       PostgreSQL 7.3 and later
    // QSQLITE     SQLite version 3
    // QSQLITE2    SQLite version 2
    // QTDS        Sybase Adaptive Server
    // QMYSQL      MySQL
    //  |
    //  +------------------------------------------+
    //                                             !

    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("localhost");
    db.setDatabaseName("course");
    db.setUserName("root");
    db.setPassword("root");
    if (!db.open()) {
        qDebug() << "Datenbank kann nicht geoeffnet werden...";
        return false;
    }

    // 1. Möglichkeit: Direktes Ausführen über SQL-Strings:
    QSqlQuery query;
    query.exec("SELECT * FROM tbl_Mitarbeiter WHERE Vorname='Donald'");

    while (query.next()) {
        qDebug() << query.value(0).toString() << query.value(1).toString() << query.value(2).toString();
    }

    // 2. Möglichkeit: Mittels einer Abstraktionsschicht (wesentlich höhrere Zeilenperformance):
    QSqlTableModel model;
    model.setTable("tbl_Mitarbeiter");
    model.setFilter("Vorname='Donald'");
    model.select();
    for (int i = 0; i < model.rowCount(); i++) {
        QSqlRecord record = model.record(i);
        qDebug() << record.value("Vorname").toString();

    }

    // Beispiel: Hinzufügen von Datensätzen:
    //QSqlTableModel model;
    //model.setTable("tbl_Mitarbeiter");
    //int row = 0;
    //model.insertRows(row, 1);
    //model.setData(model.index(row, 1), "Gustav");
    //model.setData(model.index(row, 2), "Ganz");
    //model.submitAll();

    return true;
}
