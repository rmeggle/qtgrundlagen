QT += core
QT -= gui
QT += sql

TARGET = sql
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    person.cpp

HEADERS += \
    person.h

