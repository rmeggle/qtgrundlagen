#include "worker.h"
#include <QDebug>
#include <QThread>

worker::worker()
{
    qDebug() << "Constructor von worker wurde aufgerufen";
}

void worker::run() {
    for (int i=0 ; i<=10 ; i++) {
        qDebug() << "[" << thread()->currentThreadId() << "]" << "rechne...";
        QThread::msleep(1000);
    }
    qDebug() << "[" << thread()->currentThreadId() << "]" << "Fertig!";
}
