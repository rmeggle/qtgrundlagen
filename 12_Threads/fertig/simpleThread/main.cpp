#include <QCoreApplication>
#include <QDebug>
#include "worker.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    worker threadArbeiter_1;
    worker threadArbeiter_2;

    qDebug() << "[" << a.thread()->currentThreadId() << "]" << "Starte Hauptthread";

    //arbeiter.rechne();
    threadArbeiter_1.start();
    threadArbeiter_2.start();

    threadArbeiter_1.wait();
    threadArbeiter_2.wait();


    qDebug() << "Das Hauptprogramm ist fertig";

    return a.exec();
}

