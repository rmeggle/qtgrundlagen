#include <QCoreApplication>
#include <QDebug>
#include "worker.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    worker arbeiter;

    qDebug() << "Starte Berechnung";

    arbeiter.rechne();

    qDebug() << "Das Hauptprogramm ist fertig";

    return a.exec();
}

