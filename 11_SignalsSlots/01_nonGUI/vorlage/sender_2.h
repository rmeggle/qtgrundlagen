#ifndef SENDER_2_H
#define SENDER_2_H

#include <QObject>

class sender_2 : public QObject
{
    Q_OBJECT
public:
    explicit sender_2(QObject *parent = 0);
    void doStuff();

signals:
    //.-----------------.
    //| transmit( int ) |-------->
    //'-----------------'


public slots:
    //.-----------------.
    //|                 |<--------
    //'-----------------'
};

#endif // SENDER_2_H
