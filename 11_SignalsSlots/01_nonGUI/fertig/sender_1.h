#ifndef SENDER_1_H
#define SENDER_1_H

#include <QObject>

class sender_1 : public QObject
{
    Q_OBJECT
public:
    explicit sender_1(QObject *parent = 0);
    void doSend();

signals:
    //.-------------.
    //| send( int ) |-------->
    //'-------------'
    void send( int );

public slots:
    //.-------------.
    //|             |<--------
    //'-------------'
};

#endif // SENDER_1_H
