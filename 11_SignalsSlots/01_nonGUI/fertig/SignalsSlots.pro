QT += core
QT -= gui

TARGET = SignalsSlots
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    receiver.cpp \
    sender_1.cpp \
    sender_2.cpp

HEADERS += \
    receiver.h \
    sender_1.h \
    sender_2.h

