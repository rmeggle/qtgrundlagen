#include "sender_1.h"

sender_1::sender_1(QObject *parent) : QObject(parent)
{

}

void sender_1::doSend() {

    // Sende: send(47)
    //.-------------.
    //| send( 74 )  |--------> ( Connection...)
    //'-------------'
    emit( send( 47 ) );

}

