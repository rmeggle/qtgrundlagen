#include <QCoreApplication>
#include "receiver.h"
#include "sender_1.h"
#include "sender_2.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    receiver Empfaenger;
    sender_1 Sender1;
    sender_2 Sender2;

    // Connect: Sender1(SIGNAL)<-------->Empfaenger(SLOT)
    QObject::connect( &Sender1, SIGNAL(send(int)), &Empfaenger, SLOT(get(int)) );
    //.-------------.                   .-----------.
    //| send(int)   |<----------------->| get(int)  |
    //'-------------'                   '-----------'

    // Connect: Sender2(SIGNAL)<-------->Empfaenger(SLOT)
    QObject::connect( &Sender2, SIGNAL(transmit(int)), &Empfaenger, SLOT(get(int)) );
    //.-----------------.                   .-----------.
    //| transmit(int)   |<----------------->| get(int)  |
    //'-----------------'                   '-----------'

    Sender1.doSend();
    Sender2.doStuff();

    return a.exec();
}

