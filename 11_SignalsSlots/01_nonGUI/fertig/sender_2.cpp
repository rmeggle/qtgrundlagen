#include "sender_2.h"

sender_2::sender_2(QObject *parent) : QObject(parent)
{

}

void sender_2::doStuff() {

    // Sende: transmit(47)
    //.-----------------.
    //| transmit( 11 )  |--------> ( Connection...)
    //'-----------------'
    emit( transmit( 11 ) );

}
