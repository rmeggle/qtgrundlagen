#include "receiver.h"
#include <QDebug>

receiver::receiver(QObject *parent) : QObject(parent)
{

}

//.-------------.
//| Get(int x)  |<-------- ( Connection...)
//'-------------'
void receiver::get( int x ) {

    qDebug() << "Received " << x ;
}
