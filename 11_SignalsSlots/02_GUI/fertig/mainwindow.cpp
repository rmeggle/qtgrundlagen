#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->dial,SIGNAL(valueChanged(int)),ui->horizontalScrollBar,SLOT(setValue(int)));
    connect(ui->horizontalScrollBar,SIGNAL(valueChanged(int)),ui->dial,SLOT(setValue(int)));
}

MainWindow::~MainWindow()
{
    delete ui;
}
