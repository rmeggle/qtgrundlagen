#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    // Index 0: this
    // Index 1: MyWidget1 (aus "Widget1 MyWidget1;" in mainwindow.h)
    // Index 2: MyWidget1 (aus "Widget2 MyWidget2;" in mainwindow.h)
    ui->stackedWidget->insertWidget(1, &MyWidget1);
    ui->stackedWidget->insertWidget(2, &MyWidget2);

    // Connect: MyWidget1(SIGNAL)<-------->this(SLOT)
    connect(&MyWidget1,SIGNAL(homeClicked()),this,SLOT(moveHome()));
    //.---------------.                   .-------------.
    //| homeClicked   |<----------------->| moveHome()  |
    //'---------------'                   '-------------'

    // Connect: MyWidget2(SIGNAL)<-------->this(SLOT)
    connect(&MyWidget2,SIGNAL(homeClicked()),this,SLOT(moveHome()));
    //.---------------.                   .-------------.
    //| homeClicked   |<----------------->| moveHome()  |
    //'---------------'                   '-------------'

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButtonClose_clicked()
{
    this->close();
}

void MainWindow::on_pushButtonShowWidget1_clicked()
{
    ui->stackedWidget->setCurrentIndex(1);
}

void MainWindow::on_pushButtonShowWidget2_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
}

void MainWindow::moveHome()
{
    ui->stackedWidget->setCurrentIndex(0);
}

