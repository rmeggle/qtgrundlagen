#-------------------------------------------------
#
# Project created by QtCreator 2017-10-06T13:07:35
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = StackedWidgets
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    widget1.cpp \
    widget2.cpp

HEADERS  += mainwindow.h \
    widget1.h \
    widget2.h

FORMS    += mainwindow.ui \
    widget1.ui \
    widget2.ui
