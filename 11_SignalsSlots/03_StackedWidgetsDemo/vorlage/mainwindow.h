#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QWidget>          // <----------------------
#include <QStackedWidget>

#include <widget1.h> // <----------------------
#include <widget2.h> // <----------------------

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButtonClose_clicked();
    void on_pushButtonShowWidget1_clicked();
    void on_pushButtonShowWidget2_clicked();

private:
    Ui::MainWindow *ui;

    // Erstellen der Objekte MyWidget1 und MyWidget2
    // aus den ui-Klassen. Hierfür müssen die
    // jeweiligen Header zuvor inkludiert werden.
    Widget1 MyWidget1;
    Widget2 MyWidget2;

};

#endif // MAINWINDOW_H
