#include <QCoreApplication>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    int i1=5;
    int i2=5;

    // ==	identisch
    // <=	ist kleiner (oder) gleich
    // >=	ist größer (oder) gleich
    // <	ist kleiner
    // >	ist größer
    // !=	ist ungleich

    if (i1==i2) {
        // i1 ist identisch mit i2
    } else {
        // jeder andere Fall
    }

    // Vorsicht: == und = nicht verwechseln:
    if (i1=i2) {
        // i2 wurde i1 zugewiesen. Dies gelang. Der Ausdruck ist true!
    }

    if (i1>i2) {
        // i1 ist größer aber nicht größer gleich i2
    } else if (i2>i1) {
        // i2 ist größer aber nicht größer gleich i1
    }
    // aber was wenn i1 und i2 gleich sind? Dieser Vergleich fehlt...


    // Logische Operatoren

    // !	Logisches Nicht	Resultat wahr, wenn der Operand falsch ist
    // &&	Logisches Und	Resultat wahr, wenn beide Operanden wahr sind
    // ||	Logisches Oder	Resultat wahr, wenn mindestens ein Operand wahr ist (inclusive-or)

    int i3=10;

    if (i1==i2 && i2>=i3 ) {
        // Wenn i1 und i2 identisch UND i2 größergleich i3 ist..
    } else {
        // (wenn a und b identisch) UND NICHT (c größergleich d) ist
    }

    if (i1==i2 && i2>=i3 || i2!=i3) {
        // hier kommen wir immer rein, solange i2 nicht i3 ist
    }





    return a.exec();
}

